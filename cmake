.RECIPEPREFIX = >

build: clean prepare
> g++ -o bin/week 1/zad1 src/main.cpp

prepare:   
> mkdir bin

clean:
> rm -rf bin

run:
> bin/week 1/zad1
